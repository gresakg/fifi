<?php 

namespace Fifi;

class Fifi {

	protected $name;

	protected $path;

	protected $data;

	protected $filename;

	protected $ext;

	protected $filepath;

	public function __construct($settings,$logger){
		$this->filepath = $settings['filepath'];
		$this->logger = $logger;
	}

	public function init($name) {
		$this->parse_name($name);
		
		$this->set_path();
		if(false === $this->set_data()){
			$this->logger->info("Missing json.data at ". $this->path);
			return false;
		}

		$this->set_filename();

		if(!file_exists($this->get_path())) {
			$this->logger->info("Requested path exists, but the file is not found " . $this->get_path());
			return false;
		}

		return $this;
	}

	public function get_path() {
		
		$path = $this->path.$this->get_filename();

		return $path;
	}

	public function get_filename() {
		return $this->filename;
	}

	protected function set_filename() {
		if(empty($this->version)) { 
			$filename = $this->data("_file");
			if(!empty($filename)) {
				$this->filename = $filename;
				return;
			}
			$this->version = $this->data('latest_version');
		}
		
		$this->filename = $this->name.".".$this->version.".".$this->ext;


	}

	protected function data($key) {
		if(isset($this->data->$key))
			return $this->data->$key;
		else
			return NULL;
	}

	protected function set_data() {
		$this->logger->info("The path is ".$this->path);
		if(file_exists($this->path."data.json")) {
			$data = file_get_contents($this->path."/data.json");
			$this->data = json_decode($data);
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Set path to the directory. The file is in the directory with the same name 
	 * than the file.
	 */
	protected function set_path() {
		$this->path = $this->filepath.$this->name."/";
	}

	/**
	 * Check what is requested and set version and name variables
	 * @param  string $name name passed to the route
	 * @return void      [description]
	 */
	protected function parse_name($name) {
		$parts = explode(".",$name);
		$parts = array_reverse($parts);
		if(sizeof($parts) > 1) {
			$version = array();
			$base = array();
			foreach($parts as $key => $item){
				if($key == 0) {
					$this->ext = $item;
					continue;
				}
				if(is_numeric($item)) {
					$version[] = $item;
				} else {
					$base[] = $item;
				}
			}
			$this->version = implode(".",array_reverse($version));
			$this->name = implode(".",array_reverse($base));
		}
	}
}