<?php

use Slim\Http\Request;
use Slim\Http\Response;
//use \Fifi;

// Routes

$app->get("/", function ($request,$response){

    return $this->renderer->render($response, 'index.phtml');

});

$app->get('/file/{name}', function (Request $request, Response $response, array $args) {

    $this->logger->info("File " . $args['name'] . " required.");

    $fifi = $this->fifi->init($args['name']);
    if(false === $fifi){
        $this->logger->info("Fifi couldn't find the file ".$args['name'].". ");
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $path = $fifi->get_path();

    $this->logger->info("Path ".$path);
    $mime = mime_content_type($path);
    if(file_exists($path)) {
    	$new = $response
    		->withHeader('Cache-Control','public')
    		->withHeader('Content-Description','File Transfer')
    		->withHeader('Content-Transfer-Encoding','binary')
    		->withHeader('Content-Type', $mime)
    		->withHeader('Content-Length', filesize($path))
    		->withHeader('Content-Disposition','attachment; filename="'.$fifi->get_filename().'"');
    	readfile($path);
    	return $new;
    } else {
    	$args['found'] = false;
    }

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

